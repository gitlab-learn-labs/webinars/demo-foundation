data "google_client_config" "current" {}

data "gitlab_group" "agent_group_access" {
  full_path = var.gitlab_agent_group
}

data "gitlab_project" "demo-foundation" {
  id = var.gitlab_agent_project
}

resource "gitlab_cluster_agent" "gitops-agent" {
  project = data.gitlab_project.demo-foundation.id
  name    = "gitops-agent"
}

resource "gitlab_repository_file" "gitops_agent_config" {
  project        = gitlab_cluster_agent.gitops-agent.project
  branch         = "main" // or use the `default_branch` attribute from a project data source / resource
  file_path      = ".gitlab/agents/${gitlab_cluster_agent.gitops-agent.name}/config.yaml"
  content        = <<CONTENT
# Authorize the agent to access your projects
ci_access:
  groups:
    - id: ${data.gitlab_group.agent_group_access.full_path}
CONTENT
  author_email   = var.email
  author_name    = var.name
  commit_message = "[skip-ci] feature: add agent config for ${gitlab_cluster_agent.gitops-agent.name}"
}

resource "gitlab_cluster_agent_token" "this" {
  project     = gitlab_cluster_agent.gitops-agent.project
  agent_id    = gitlab_cluster_agent.gitops-agent.agent_id
  name        = "my-agent-token"
  description = "Token for the my-agent used with `gitlab-agent` Helm Chart"
}

resource "helm_release" "gitlab_agent" {
  depends_on = [
    google_container_cluster.primary
  ]
  name             = gitlab_cluster_agent.gitops-agent.name
  namespace        = "gitlab-agent"
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  //version          = "1.2.0"

  set {
    name  = "config.token"
    value = gitlab_cluster_agent_token.this.token
  }
}