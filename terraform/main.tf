
resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = var.google_region

  # Use AutoPilot for ease of management
  enable_autopilot = true
  vertical_pod_autoscaling {
    enabled = true
  }
}
