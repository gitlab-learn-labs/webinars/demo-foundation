# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.16.0"
  constraints = "3.16.0"
  hashes = [
    "h1:tjp2fd/TvdDExsrnuK1frkf8D+GE+vhyrOveodTm3fw=",
    "zh:018f7ff6e98f2a8b19b652bbc8b3eb6369a1800c4d6fe04c34c52dfa14ae9658",
    "zh:155e63d1ee7b0ba05103d12f310e5ce2aa433632de27acfccd490d827c1b2007",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:211c56fbb182ec4cba35288197263c146fc101831b981e2c14c246b690a5d04f",
    "zh:40d46193dfdd7b640654b2dc76dc32322f9dc573b1e33d6aa5f9f46cd396293b",
    "zh:4405d8963237e41aa7ff4146a10e50257314d552647e3cf3267001a469e787a1",
    "zh:4d96ae31a4ea2ee07942863df284524710cc89d1b6ba440f2c93d70b043d0571",
    "zh:66d11d98360bb0aba8e2ad14fdda149442b49b0e91095410c75eb1102f1dbda6",
    "zh:78d5dffcfe9e9f45a7b5cb6c2cdf4d686d2db79cd2688ef781e24c83c3e9cd9f",
    "zh:a343ed08c905cfb42f3438620fabf9be964917a670f1555a1885dc995994a3fa",
    "zh:c4760f46ee81018dc1da22678a0c44227f57610e5a8d93f08ccde9eecf2f3837",
    "zh:c721b82dbafb15bb088372a39a5564363e71a9947fa709fb25bb7aa5dd2a3d62",
    "zh:c838136aa7e6ea8541918609f2490bbff977c45588c39b0db1d736e965c4ff78",
    "zh:d3a6e753fb84e5d566a10d6e02bce4d6c41d1bf7085ef02b59d1013db73659a9",
    "zh:d88a434b4bc37a47ce95f9f9a5559a53bc431cba76244ccf405c47b9f86f813c",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.0.0"
  constraints = "~> 4.0.0"
  hashes = [
    "h1:F+3tjVSHtZO21LiU2pekR3//7hPKTpB8WYZN1jLd86I=",
    "zh:48fbf905a5c1658b042c7edb0183e9a25dc04257cd5e6d4d870b33a0bc70eda4",
    "zh:5f7a1579e23f0d187cc9062ec54f30d7336252705d4e0dd9dbd799cebd83429b",
    "zh:63bb8f094eec5cecf1acbe7026c0ba31126670282dbcfce01477aec6c134a3bd",
    "zh:7654b20e5dc0ed595f084039e8fd49ff70c11e2ed7b9fcc9c6169f3217845adb",
    "zh:7bc37430171b2148b2bb835dc2eff967714d2ac515e73784b66c2ddda6d260fa",
    "zh:84212e750b3a319055ee0dc43ef40a1096fea6793af8d87aecc330ead4e8e9c9",
    "zh:ac53b6cd95fc024b96dba46e204bbc4963e744c7017f11d454ba49a4a8355ee3",
    "zh:cc6a4989874fc7a1d3b82f8be761867c85472b37c5656a81cee8385330a5afa2",
    "zh:d1ffc766f855995b6117f7f40ec02e2404a8474630dbcc38b8355519af1a47f5",
    "zh:e68183a7c0f1efec3e36931ca06d31060218a9d44379aad5269e9d6fc6e6f47f",
    "zh:e7c0dbfae422f8ae663c3462d549ac69287038fc880bfad5c58790b82bfde620",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.8.0"
  hashes = [
    "h1:a98mBNghv9odh5PVmgdXapgyYJmO/ncAWkwLWdXLuY4=",
    "zh:1e42d1a04c07d4006844e477ca32b5f45b04f6525dbbbe00b6be6e6ec5a11c54",
    "zh:2f87187cb48ccfb18d12e2c4332e7e822923b659e7339b954b7db78aff91529f",
    "zh:391fe49b4d2dc07bc717248a3fc6952189cfc49c596c514ad72a29c9a9f9d575",
    "zh:89272048e1e63f3edc3e83dfddd5a9fd4bd2a4ead104e67de1e14319294dedf1",
    "zh:a5a057c3435a854389ce8a1d98a54aaa7cbab68aca7baa436a605897aa70ff7e",
    "zh:b1098e53e1a8a3afcd325ecd0328662156b3d9c3d80948f19ba3a4eb870cee2b",
    "zh:b676f949e8274a2b6c3fa41f5428ea597125579c7b93bb50bb73a5e295a7a447",
    "zh:cdf7e9460f28c2dbfe49a79a5022bd0d474ff18120d340738aa35456ba77ebca",
    "zh:e24b59b4ed1c593facbf8051ec58550917991e2e017f3085dac5fb902d9908cb",
    "zh:e3b5e1f5543cac9d9031a028f1c1be4858fb80fae69f181f21e9465e366ebfa2",
    "zh:e9fddc0bcdb28503078456f0088851d45451600d229975fd9990ee92c7489a10",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.16.1"
  hashes = [
    "h1:kO/d+ZMZYM2tNMMFHZqBmVR0MeemoGnI2G2NSN92CrU=",
    "zh:06224975f5910d41e73b35a4d5079861da2c24f9353e3ebb015fbb3b3b996b1c",
    "zh:2bc400a8d9fe7755cca27c2551564a9e2609cfadc77f526ef855114ee02d446f",
    "zh:3a479014187af1d0aec3a1d3d9c09551b801956fe6dd29af1186dec86712731b",
    "zh:73fb0a69f1abdb02858b6589f7fab6d989a0f422f7ad95ed662aaa84872d3473",
    "zh:a33852cd382cbc8e06d3f6c018b468ad809d24d912d64722e037aed1f9bf39db",
    "zh:b533ff2214dca90296b1d22eace7eaa7e3efe5a7ae9da66a112094abc932db4f",
    "zh:ddf74d8bb1aeb01dc2c36ef40e2b283d32b2a96db73f6daaf179fa2f10949c80",
    "zh:e720f3a15d34e795fa9ff90bc755e838ebb4aef894aa2a423fb16dfa6d6b0667",
    "zh:e789ae70a658800cb0a19ef7e4e9b26b5a38a92b43d1f41d64fc8bb46539cefb",
    "zh:e8aed7dc0bd8f843d607dee5f72640dbef6835a8b1c6ea12cea5b4ec53e463f7",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fb3ac4f43c8b0dfc0b0103dd0f062ea72b3a34518d4c8808e3a44c9a3dd5f024",
  ]
}
