
variable "google_project" {
  description = "The ID of the GCP project where the resources will be deployed."
  type        = string
}

variable "google_region" {
  description = "The region in GCP where the resources will be deployed."
  type        = string
  default     = "us-central1"
}

variable "google_zone" {
  description = "The zone in the above region where the resources will be deployed."
  type        = string
  default     = "us-central1-a"
}

variable "username" {
  description = "The username for SSH access to the compute instance"
  type        = string
}

variable "name" {
  description = "The name of the user who started the pipeline, unless the job is a manual job. In manual jobs, the value is the name of the user who started the job."
  default     = "John Doe"
  type        = string
}

variable "email" {
  description = "The email of the user who started the pipeline, unless the job is a manual job. In manual jobs, the value is the email of the user who started the job."
  default     = "john@doe.com"
  type        = string
}

variable "cluster_name" {
  description = "The name of the cluster"
  default     = "my-amazing-gke-cluster"
  type        = string
}

variable "gitlab_agent_project" {
  description = "The project we are using for the agent"
  type        = string
}

variable "gitlab_agent_group" {
  description = "The Group the agent gives access to"
  type        = string
}

variable "gitlab_token" {
  description = "The access token used to create variables in the project. Must be at the group level, not the project, and have the maintainer role and the api scope."
  type        = string
}
